package com.example.springmonthlyexam.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Movie {
    int id;
    String title;
    String description;
    String releasedYear;
}
