package com.example.springmonthlyexam.repository;

import com.example.springmonthlyexam.model.Movie;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovieRepository {


    public List<Movie> findAll();
    Movie deleteBiId(int id);
    Movie save(Movie movie);

}
