package com.example.springmonthlyexam.controller;

import com.example.springmonthlyexam.model.Movie;
import com.example.springmonthlyexam.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class MovieController {

    private MovieService movieService;

    @Autowired
    public void setMovieService(MovieService movieService) {
        this.movieService = movieService;
    }

    @GetMapping("/movies/list")
    public ResponseEntity<?> getAllMovie(Model model){
        Map<String, Object> response = new HashMap<>();

        List<Movie> movieList = movieService.findAll();

        model.addAttribute("movieList", movieList);

        response.put("status", "OK");
        response.put("message", "get movie success");
        response.put("payload", movieList);

        return ResponseEntity.ok().body(response);
    }

    @DeleteMapping("/movies/{id}/delete")
    public ResponseEntity<?> deleteMovieById(@PathVariable int id){
        Map<String, Object> response = new HashMap<>();

        Movie movie = movieService.deleteBiId(id);

        response.put("status", "OK");
        response.put("message", "delete success");
        response.put("payload", movie);

        return ResponseEntity.ok().body(response);
    }

    @PostMapping("/movies/save")
    public ResponseEntity<?> save(@ModelAttribute Movie movie){
        Map<String, Object> response = new HashMap<>();

        Movie movie1 = movieService.save(movie);

        response.put("status", "OK");
        response.put("message", "save success");
        response.put("payload", movie1);

        return ResponseEntity.ok().body(response);
    }
}
