package com.example.springmonthlyexam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMonthlyExamApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringMonthlyExamApplication.class, args);
    }

}
