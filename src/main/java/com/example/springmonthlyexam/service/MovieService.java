package com.example.springmonthlyexam.service;

import com.example.springmonthlyexam.model.Movie;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface MovieService {

    public List<Movie> findAll();
    Movie deleteBiId(int id);
    Movie save(Movie movie);
}
